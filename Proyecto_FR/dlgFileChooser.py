#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')  # noqa: E402
from gi.repository import Gtk


# Se crea la clase que permite la interaccion con botones aceptar y cancelar
class dlgFileChooser():
    def __init__(self, title=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        # Ventana diálogo file chooser.
        self.dialog = self.builder.get_object("chooserDialog")
        self.dialog.set_title(title)
        self.dialog.resize(600, 400)

        # Ruta
        self.path = None

        # Botón aceptar.
        button_ok = self.dialog.add_button(Gtk.STOCK_OK,
                                           Gtk.ResponseType.OK)
        button_ok.set_always_show_image(True)
        button_ok.connect("clicked", self.button_ok_clicked)

        # Botón cancelar.
        button_cancel = self.dialog.add_button(Gtk.STOCK_CANCEL,
                                               Gtk.ResponseType.CANCEL)
        button_cancel.set_always_show_image(True)
        button_cancel.connect("clicked", self.button_cancel_clicked)

        # Cuando el usuario presiona Ok, se obtiene el directorio
    def button_ok_clicked(self, btn=None):
        print("Aceptar - dlgFileChooser")
        # Obtiene directorio seleccionado.
        self.path = self.dialog.get_current_folder()

        # Cuando el usuario presiona cancelar el dialogo se destruye
    def button_cancel_clicked(self, btn=None):
        print("Cancelar - dlgFileChooser")
        self.dialog.destroy()
