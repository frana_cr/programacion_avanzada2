#!/usr/bin/env python
# -*- coding: utf-8 -*-

import __main__
__main__.pymol_argv = ["pymol", "-qc"]
import pymol
pymol.finish_launching()


# Se crea la funcion que genera la imagen con sus caracteristicas
def gen_image(name, file, path_save):
    # Generar imágenes de archivo seleccionado.
    aux = name.split(".")
    pdb_name = aux[0]
    png_file = "".join([path_save])
    pdb_file = file
    pymol.cmd.load(pdb_file, pdb_name)
    pymol.cmd.disable("all")
    pymol.cmd.enable(pdb_name)
    pymol.cmd.hide("all")
    pymol.cmd.show("cartoon")
    pymol.cmd.set("ray_opaque_background", 0)
    pymol.cmd.pretty(pdb_name)
    pymol.cmd.png(png_file)
    pymol.cmd.ray()
