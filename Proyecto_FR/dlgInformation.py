#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import gi
gi.require_version('Gtk', '3.0')  # noqa: E402
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from dlgMessage import dlgMessage


# Se importa la clase encargada de mostara la informacion al usuario
class dlgInformation():
    def __init__(self, title="", path=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        # Cargar ruta del archivo.
        self.path = path

        # Ventana de diálogo.
        self.dialog = self.builder.get_object("proteinViewer")
        self.dialog.resize(800, 600)
        self.dialog.set_title(title)

        # Botón guardar.
        button_save = self.builder.get_object("buttonSave")
        button_save.connect("clicked", self.button_save_clicked)

        # Botón cancelar.
        button_cancel = self.builder.get_object("buttonCancel")
        button_cancel.connect("clicked", self.button_cancel_clicked)

        # Combobox.
        self.combobox = self.builder.get_object("combobox")
        render_text = Gtk.CellRendererText()
        self.combobox.pack_start(render_text, True)
        self.combobox.add_attribute(render_text, "text", 0)
        self.model = Gtk.ListStore(str)
        self.combobox.connect("changed", self.combobox_changed)

        # Determinar opciones del combobox.
        list = ["ATOM", "HETATM", "ANISOU", "OTHERS"]
        for index in list:
            self.model.append([index])

        # Agregar opciones a combobox.
        self.combobox.set_model(self.model)

        # Selección del combobox.
        self.cmb_value = None

        # Label.
        self.data = self.builder.get_object("infoProtein")
        self.data.set_max_width_chars(1)
        self.data.set_hexpand(True)

    # Funcion encargada de obtener la opcion del usuario respecto al combobox
    def combobox_changed(self, cmb=None):
        it = self.combobox.get_active_iter()
        model = self.combobox.get_model()
        self.cmb_value = model[it][0]
        # Generar datafragmento requerido.
        ppdb = PandasPdb()
        ppdb.read_pdb(self.path)
        df = ppdb.df[self.cmb_value].head()
        # Filtrar por datos de importancia
        df = df.filter(["record_name", "atom_number", "atom_name",
                        "residue_name", "element_symbol", "charge"])

        # Si el dataframe contiene información, se añade al label.
        if ppdb.df[self.cmb_value].empty is False:
            df_str = df.to_string(justify="right", header=True, index=True)
            self.data.set_text(df_str)
        else:
            self.data.set_text("SIN INFORMACIÓN.")

    # Funcion encargada cuando el usuario presiona guardar
    def button_save_clicked(self, btn=None):
        ppdb = PandasPdb()
        ppdb.read_pdb(self.path)
        name = self.path
        name = name.split(".")
        fragment = self.cmb_value
        path_save = "".join([name[0], "_", fragment, ".pdb"])

        # Comprobar existencia del archivo.
        exist = os.path.isfile(path_save)
        dlg = dlgMessage()

        # Se guarda el archivo, y se establecen ciertas condiciones
        if exist is False:
            if ppdb.df[fragment].empty is False:
                ppdb.to_pdb(path=path_save,
                            records=[fragment],
                            gz=False,
                            append_newline=True)
                # El combobox seleccionado es guardado
                dlg.dialog.set_property("text",
                                        "¡Información guardada exitosamente!")
                response = dlg.dialog.run()
                # El combobox seleccionado no tiene informacion
            else:
                dlg.dialog.set_property("text",
                                        "¡Sin información para guardar!")
                response = dlg.dialog.run()
                # El combobox seleccionado ya existe en el directorio
        else:
            dlg.dialog.set_property("text",
                                    "¡El archivo ya existe!")
            response = dlg.dialog.run()

        # Destruir mensaje.
        if response == Gtk.ResponseType.OK:
            dlg.dialog.destroy()

    def button_cancel_clicked(self, btn=None):
        self.dialog.destroy()
