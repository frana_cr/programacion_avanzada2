#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
import os
import gi
gi.require_version('Gtk', '3.0')  # noqa: E402
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from dlgFileChooser import dlgFileChooser
from dlgInformation import dlgInformation
from dlgAbout import dlgAbout
from dlgMaximize import dlgMaximize
from fileImage import gen_image


# Se crea la clase de la ventana principal
class wnPrincipal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        # Ventana principal.
        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("VISUALIZADOR ARCHIVOS PDB")
        self.window.maximize()
        self.window.show_all()

        # Ruta.
        self.folder_path = None

        # Label directorio.
        self.dir = self.builder.get_object("directory")

        # Label info.
        self.info = self.builder.get_object("info")

        # Imagen.
        self.imagen = self.builder.get_object("image")
        # self.imagen.clear()

        # Botón para abrir directorio.
        button_open = self.builder.get_object("buttonOpen")
        button_open.connect("clicked", self.button_open_clicked)

        # Botón para visualizar archivo.
        button_view = self.builder.get_object("view")
        button_view.connect("clicked", self.button_view_clicked)

        # Botón para maximizar imagen de proteína.
        button_maximize = self.builder.get_object("maximize")
        button_maximize.connect("clicked", self.button_maximize_clicked)

        # Botón información.
        button_about = self.builder.get_object("about")
        button_about.connect("clicked", self.button_about_clicked)

        # Liststore.
        self.liststore = self.builder.get_object("tree")
        self.model = Gtk.ListStore(*(1 * [str]))
        self.liststore.set_model(model=self.model)

        # RendenText
        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Archivo(s) PDB",
                                    cell_renderer=cell,
                                    text=0)

        self.liststore.append_column(column)

        self.selection = self.liststore.get_selection()
        self.selection.connect("changed", self.on_changed)

        # Esta funcion es la encargada de mostrar imagen e informacion previa
    def on_changed(self, selection):
        model, it = selection.get_selected()

        if it is not None:
            # Obtener ruta del archivo.
            file = "/".join([self.folder_path, model[it][0]])

            # Cargar texto de visualización previa.
            ppdb = PandasPdb()
            ppdb.read_pdb(file)
            text = ppdb.pdb_text[:1000]
            self.info.set_text(text)

            # Generar imagen de la proteína.
            name = model[it][0].split(".")
            path_save = "/".join([self.folder_path, "imagenPDB"])
            path_save = "/".join([path_save, name[0]])
            path_save = "".join([path_save, ".png"])

            # Verificar existencia de imagen en la carpeta.
            exist = os.path.isfile(path_save)

            # Si no existe, se genera la imagen.
            if exist is False:
                gen_image(model[it][0], file, path_save)

            # Cargar imagen a la ventana principal.
            self.imagen.set_from_file(path_save)

            # Esta funcion sobre la visualizacion de mayor informacion
    def button_view_clicked(self, btn=None):
        # Visualiza información sobre un archivo seleccionado.
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        # Ruta del archivo .pdb
        file = "/".join([self.folder_path, model[it][0]])

        dlg = dlgInformation(title=model[it][0], path=file)
        dlg.dialog.run()

        # Esta funcion permite obtener la imagen maximizada
    def button_maximize_clicked(self, btn=None):
        # Visualizar imagen de proteína.
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return

        dlg = dlgMaximize(title=model[it][0])
        name = model[it][0].split(".")
        path = "/".join([self.folder_path, "imagenPDB"])
        path = "/".join([path, name[0]])
        # Agregar imagen a la visualización.
        dlg.max_img.set_from_file("".join([path, ".png"]))
        dlg.dialog.run()

        # Esta funcion muestra informacion acerca de la aplicacion
    def button_about_clicked(self, btn=None):
        dlg = dlgAbout(title="Info")
        dlg.dialog.run()
        dlg.dialog.destroy()

        # Funcion selecciona un directorio y obtiene la informacion de proteina
    def button_open_clicked(self, btn=None):
        file_chooser = dlgFileChooser(title="Seleccionar directorio")
        response = file_chooser.dialog.run()

        if response == Gtk.ResponseType.OK:
            # Se obtiene directorio seleccionado.
            self.folder_path = file_chooser.path
            self.dir.set_text(self.folder_path)
            self.info.set_text("")
            # Se filtran archivos de formato .pdb presentes en el directorio.
            file_list = list(pathlib.Path(self.folder_path).glob('*.pdb'))

            if file_list != []:
                # Crear carpeta para guardar imágenes.
                path = "/".join([self.folder_path, "imagenPDB"])
                os.makedirs(path, exist_ok=True)

                # Recorrer lista para agregar nombres.
                for f in file_list:
                    text = os.path.split(f)
                    # Añadir nombre de archivo a la ventana.
                    aux = str(text[1])
                    # añadir nombre de arcivo a la ventana
                    self.model.append([aux])

                file_chooser.dialog.destroy()


# Se llama al main y funciones que se desarrollaran
if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()
