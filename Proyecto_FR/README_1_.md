# PDB Science For You

PDB Science For You es un visualizador interactivo de proteínas en formato pdb. Permite apreciar la estructura 3D de las proteínas, con un estilo de representación cartoon; observar la información referida a ítems como “ATOM", "HETATM", "ANISOU" y "OTHERS"; y guardar aquel fragmento seleccionado en un archivo de formato txt.

## Comenzando
Este proyecto consiste en la ejecución de una ventana principal que contiene un menú con cuatro opciones: “abrir”, “visualizar información”, “maximizar imagen” y “acerca de”. Primeramente, al presionar la opción “abrir”, se instancia una ventana en la que el usuario puede seleccionar un directorio del cual cargar archivos .pdb en caso de que existan los mismos; si hay archivos del formato mencionado, aparecerán sus nombres registrados en el listado de la ventana principal. Luego, si el usuario selecciona alguno de ellos, se ofrecerá una vista previa de los primeros 1000 caracteres contenidos en el archivo y una imagen de la estructura 3D de la proteína, la cual se registrará en una carpeta de nombre “imagenPDB” dentro del mismo directorio seleccionado. En segundo lugar, el usuario puede presionar la opción “visualizar información” y se desplegará una nueva ventana siempre que se esté seleccionado un archivo; dicha ventana permitirá elegir un fragmento de texto para visualizar, siendo “ATOM", "HETATM", "ANISOU" y "OTHERS" las posibilidades. Asimismo, dentro existe la opción de guardar el fragmento seleccionado, para lo que el usuario deberá presionar el botón “guardar” dentro de la ventana. Si el archivo se guarda correctamente, se muestra una ventana confirmando la acción; en caso de que el archivo ya haya sido generado, se muestra una ventana indicando que el archivo ya existe. En añadidura, si el fragmento no posee información, se presenta una ventana para indicarlo. En tercer lugar, la opción “maximizar imagen” permitirá observar la representación 3D en otra ventana.

## Pre-requisitos
* Sistema operativo Linux versión igual o superior a 18.04
* Pymol
* Python3
* Biopandas 

## Instalación
1. Para ejecutar el programa se debe conocer qué versión de Ubuntu presenta la computadora. 
  * Ejecutar comando: lsb_release -a (versión de Ubuntu)
  * En donde: Se corrobora qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando. 
  
2. Si la versión de Ubuntu es superior a 18.04, no debería problemas con las importaciones de librería pymol, necesaria para el desarrollo del programa y la visualización de las proteínas (obtiene el generado de la imagen del archivo PDB). Se puede instalar con el siguiente comando:
  * sudo apt-get install python3-pymol
  * En donde: Se descarga pymol para poder visualizar las estructuras sin problemas, en cuanto a la visualización del objeto indicado por el usuario.

3. Si la versión de Ubuntu es inferior o igual a 18.04 puede ser incompatible con el uso de pymol, herramienta que permite la generación de una imagen desde un archivo local PDB. En este último caso, se deben seguir los siguientes pasos para poder obtener pymol:
  * Obtener el archivo pymol.bash y luego ejecutarlo por medio del comando bash pymol.bash.
  * En donde: Se importa pymol desde un archivo previamente descargado, sin tener problemas con la versión actual de la computadora. La importancia de la herramienta de pymol radica en que permite  la visualización digital de moléculas y macromoléculas de los sistemas biológicos de modo tridimensional.

4. Corroborar la versión de Python disponible actualmente para poder ejecutar el código sin errores. 
  * Para conocer qué versión se está usando, se debe ingresar el siguiente comando en la terminal: python3
  * En donde: Se obtiene la versión de actual Python disponible. 
  * Si es superior a 3, el programa no presentará errores en su ejecución. Pero si es inferior, se debe:
   * Descargar desde este sitio la última versión de Python https://www.python.org/downloads/
   * Ejecutar el siguiente comando tar xvf python-3.8.5.tgz cd Python-3.8.5 ./configure --prefix= make -j4 make install

5. BioPandas es un paquete dentro de Python, el cual permite trabajar con las estructuras biológicas moleculares con ayuda de Pandas. En este caso, se puede obtener información, seleccionar una porción específica, clasificar y trabajar el DataFrame que se genera con un archivo PDB. Para poder importarlo sin problemas en el código, se debe ejecutar el siguiente comando en la terminal:
  * pip3 install biopandas
  * En donde: Se puede importar Biopandas de manera óptima, esto con el fin de que el usuario no tenga problemas al obtener la información que requiera con respecto a la proteína de interés. 

### Consejo extra
Con el objetivo de ejecutar la interfaz de la aplicación, se debe verificar que en python3 esté instalada la librería gráfica Gtk. Para corroborar que exista, se debe ejecutar:
* python3
* import gi

* En donde: El usuario puede verificar que el programa no presente errores para tener éxito en la ejecución, en conjunto con Gtk/Gnome.

Si tiene problemas con la instalación de este se deben ejecutar los comandos:
* sudo apt-get install python3-gi

## Ejecutando las pruebas 

Para implementar la ejecución del código, colocar en terminal: python3 wnPrincipal.py

## Construido con
* Ubuntu: Sistema operativo.
* Pymol: Herramienta que permite la visualización molecular.
* Python: Lenguaje de programación.
* Biopandas: Librería para trabajar con estructuras moleculares en DataFrames.
* GTK: Librería que contiene herramientas de interfaces gráficas.
* Glade: Herramienta para desarrollar interfaces gráficas.
* Atom: Editor de código.

## Versiones
#### Versiones de herramientas:
* Ubuntu 20.04 LTS
* Pymol 2.4.0
* Python 3.8.2
* Biopandas 0.2.5
* GTK 3.24.18
* Glade 3.22.2
* Atom 1.46.0

#### Versiones del desarrollo del codigo: 
* https://gitlab.com/frana_cr/programacion_avanzada2/-/tree/master/Proyecto_FR
* https://gitlab.com/frana_cr/proyecto2-2020-pa

## Autores 
* Francisca Castillo - Desarrollo del código y proyecto, narración README.
* Rocío Rodríguez - Desarrollo del código y proyecto, narración README.

## Expresiones de Gratitud
* A los ejemplos en la plataforma de GitLab de Fabio Durán Verdugo: https://gitlab.com/fabioduran/programacion-avanzada-2020/-/tree/master/unidad1/gtk 
* Sebastian Raschka: http://rasbt.github.io/biopandas/tutorials/Working_with_PDB_Structures_in_DataFrames/

