#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')  # noqa: E402
from gi.repository import Gtk


# Se crea la clase que entrega al usuario el dialogo sobre guardar archivo
class dlgMessage():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        self.dialog = self.builder.get_object("fileSaved")
