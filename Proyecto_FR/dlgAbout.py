#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')  # noqa: E402
from gi.repository import Gtk


# Se crea la clase que muestra informacion acerca de la aplicacion
class dlgAbout:
    def __init__(self, title=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        # Ventana "acerca de".
        self.dialog = self.builder.get_object("infoApp")
        self.dialog.set_title(title)
        self.dialog.resize(400, 600)
