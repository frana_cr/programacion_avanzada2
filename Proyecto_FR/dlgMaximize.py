#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')  # noqa: E402
from gi.repository import Gtk


# Se importa la clase encargada de maximizar la imagen previa
class dlgMaximize():
    def __init__(self, title=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")
        self.dialog = self.builder.get_object("maximizePicture")
        self.dialog.set_title(title)
        self.dialog.resize(400, 600)

        # Imagen.
        self.max_img = self.builder.get_object("maxImage")

        # Botón aceptar.
        button_max = self.dialog.add_button(Gtk.STOCK_OK,
                                            Gtk.ResponseType.OK)
        button_max.set_always_show_image(True)
        button_max.connect("clicked", self.button_max_clicked)

        # Funcion que destruye el dialogo
    def button_max_clicked(self, btn=None):
        self.dialog.destroy()
